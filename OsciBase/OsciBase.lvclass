﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="19008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">OsciDriver.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../OsciDriver.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the base class of the Oscilloscope Driver.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!*+!!!*Q(C=\&gt;5^4C."%)&lt;BT[N&amp;)P5.E%03%D@Q(M&amp;8K#MY*@1&amp;.KAL/#*;#4EBWB_I+`A+PE,T&gt;F-9MU)9;2@N"MT1:PRV&gt;`8$4'O1RH%K@69\&gt;FS`?LSB`W")`^*_[W]^0BBRX6\I(Z^N0\_KNK@_BWFVU1YOWJ\Q&lt;(Y6@)P`$`O0(0IY0IZ`?VQ?X;4PPG=H[C]C7N+#ZD248[IF?:)H?:)H?:)(?:!(?:!(?:!\O:-\O:-\O:-&lt;O:%&lt;O:%&lt;O:'0EVTE)B=ZJ'4R:+'E;&amp;)A'1R&amp;S5@C34S**`(Q69EH]33?R*.Y'+,%EXA34_**0%R4YEE]C3@R*"Z+$5G/ERR0YK']!E`A#4S"*`#QJ!*0!!A7#QI(27!I[!QO!E`A#4R=+P!%HM!4?!)0X1I]A3@Q"*\!QZ2R6W*I_EG/BT*S0)\(]4A?RU.J/2\(YXA=D_.B/4E?R_-AH!7&gt;YB$E4()'/&amp;]=D_0BFRS0YX%]DM@RU$7?E)]\UT8^*-&gt;D?!S0Y4%]BI=3-DS'R`!9(M.$72E?QW.Y$)`B93E:(M.D?!S)M3D,SSBG4$1''9(BY7@M&amp;BN0+9&lt;%RCH6ZF6N3N6G5WUCV?:10846QV1^*.8.6^V5V=V3X146([&gt;#KT#K2634_U$N_.T3.L1V&lt;56&lt;UB;U/7V'G`;J@XHA&lt;L@4&gt;LP6:L02?LX7;L83=LH59L(1@$\8&lt;$&lt;4&gt;$L&gt;PQ&lt;//0=PB)@XUC88P6W&gt;X%R_4MYG&gt;R@@@^V_/@`[\?*(00&lt;V^E\`3`_$&gt;[-_[&lt;!O^_A?L&amp;14^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">419463168</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.6</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!##^5F.31QU+!!.-6E.$4%*76Q!!(#Q!!!32!!!!)!!!(!Q!!!!G!!!!!B"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!!!!!!+!:!)!!!$!!!!A!"!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!&amp;.FMH&lt;HFT5_@C-=L=LL"$A!!!!Q!!!!1!!!!""YY4`E^:1&gt;#CE*X5!?&gt;'-(5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!$3^12]V0Q-39;X)*+&amp;(OD"!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%.&amp;:G]&amp;I)?5C]+Q+MYPM^J1!!!!%!!!!!!!!!)E!!5R71U-!!!!#!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!*735.$!!!!!!%15W6N98"I&lt;X*F)&amp;*F:EZV&lt;1"16%AQ!!!!-A!!!!1(0(:J&lt;'FC0A&gt;6&gt;'FM;82Z$(.F&lt;7&amp;Q;'^S,GRM9B"4:7VB='BP=G5A5G6G4H6N!!!!!!!"1A!!!!!!!Q!!!!!!!!)!!A!!!!!!+!!!!#*YH'0A:'"O9,D!!-3-$%Q.8%!7EQ9$G'&lt;YQ-$!)=!!!)1\"Z9!!!!5!!!!$HC=9W"FY'(AA%)'!!'4!$)!!!",!!!"'(C=9W$!"0_"!%AR-D!Q]Q&amp;J.D2R-!VD5R0A-B?886"R:C"G!7*7G$!$!^-?)-U%%I?K592)-&lt;U"YB0IZP"$[1N)9A$&lt;8#B]!!!!!!Q!!6:*2&amp;-!!!!!!!-!!!'#!!!$3(C=+W"E9-AUND!\!+3:A6C3I9%B/4]FF:="S'?!A$&gt;-$"3$!+BZ7GDCBA=/JQ'"(L^]#ZD@\;,#UFSDQM.5SP?`2)5DY!6)M0E)R_&amp;ODZTDDD:A*2R:$&amp;E-!@]$-ZK0])"V)_H09A"K%Q&amp;BK%Y?.0.^6&amp;A-$T25+D/5#B^P.''%O#)1KJDF-%(^10="T?!Y_*#F&gt;S*9I".%&gt;I9Q3BRX9&gt;12!\*\?2A$%?Z'=V^9.^"879RB-0FONO-/'C$W=1=2#*5"I3IA6!')WA%CYAZD#^?VL_`N9A83&lt;%BC$F$=!-3A?)6B01:'"J#(G9#Q!UD`_@``PQV1B!EKJAA6!\&amp;P1NG-$0:Q0&lt;/B9BJ)ZPS%[U'ICY;+/3#Z"W1(S+;^1&amp;I$SDY-:4&gt;!X1]39Q5;-A(+ZA'S#["M93"\!Z1N"71,1.G+109(+&amp;M.SDY!D66UWNH@R25Z\%$J'J&lt;'J9%Y/&lt;@!Q%#P/FA("EO&gt;&gt;-*UCN*UAGM:J%$S"=FF/+1:!"&gt;FEX]!!!!!!4-!!!+]?*RT9'"AS$3W-'.A9G"A:G2AE'2I9%D/4UFF1!)N4!QY18B9]RO/\BI6C?Y3&amp;:(/%B774BO6`_&lt;`T6[!**O0=(1?.$T1`*/2@]J"`N9YI0G^&lt;CS&gt;,CIMP?YMH3QK`]N:`J?R9&amp;.K$&amp;4;$6,HQ&gt;,JI],3T;03']D3+;,SPZ$F@Q&amp;7(9QE[TD01+K/O4BU&gt;!7Q'"[Q#G1"+CKNA3BAB#NA""P*_,_!%&gt;H)VJ-AR@\94/M'G79.-5W0M!-.4T&lt;`!$GP]]2LJIY41)'Y_-/YYQM&gt;L(V^&lt;R=I?BG2R"S!7)F"AI%.3)0E*)(YT````U&amp;M&amp;S2W'2*\"2,\&amp;B+\GB(#,A&lt;;Q!AV4R=KZA#VT^H@R25^P9(53A.R=G["A9&amp;?&gt;&lt;!/$*9[[94J&amp;+8J".=S!!#KI\I!!!!!!1I!!!)]?*RT9'"AS$3W-'NA:'"A"G**BA;'Z0S56!9EQ-,%A"/%BT7`Y?CO5:(I,F%2[3R29?GU51H`&lt;`9#*.&gt;]B+0V!(`L73#TVYWFUU7&amp;J@EY2,[-"6H"'C#T'SD&lt;[](3[;0#UMWDUHS1J:.(*@"`!9K[&amp;K,5F391I=9'65VP)%OHC%K8!UO()QN%BB%KYQD2SYD17])'J/,C$W-TI&gt;O"J2.G!E\\"6Y@2)4@WN@X&gt;I'#FR%J4"W!/!MIQA;E18)310TH````),9F%DM2C&gt;W*R&amp;[,R.:FB,",A/9R1MV\#:6XA.LH\/`CCB\@),631*R=E&amp;SG6RWM!Y/F4DJB/E6J/M'V$!Q!:9K2,1!!!!!!$BE"A!=!!!9R/3YQ,D%!!!!!!!!-'1#!!!!!"$%Z,D!!!!!!$BE"A!=!!!9R/3YQ,D%!!!!!!!!-'1#!!!!!"$%Z,D!!!!!!$BE"A!=!!!9R/3YQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#ZO1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!#ZU=8,U&lt;E!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!#ZU=7`P\_`S^'Z!!!!!!!!!!!!!!!!!!!!!0``!!#ZU=7`P\_`P\_`P]P2O1!!!!!!!!!!!!!!!!!!``]!S]7`P\_`P\_`P\_`P\`,U1!!!!!!!!!!!!!!!!$``Q$&amp;R&lt;_`P\_`P\_`P\_`P``,!!!!!!!!!!!!!!!!!0``!-8,S]7`P\_`P\_`P````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P&amp;P\_`P```````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,R&gt;(````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$,S]P,S]P,S````````]P,!!!!!!!!!!!!!!!!!0``!!$&amp;R=P,S]P,`````]P2R1!!!!!!!!!!!!!!!!!!``]!!!!!R=P,S]P``]P,R1!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!-8,S]P,PQ!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!$&amp;PQ!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!A1!"2F")5!!!!!%!!F2%1U-!!!!"%&amp;.F&lt;7&amp;Q;'^S:3"3:7:/&gt;7U!5&amp;2)-!!!!$)!!!!%"TRW;7RJ9DY(682J&lt;'FU?1RT:7VB='BP=CZM&lt;')15W6N98"I&lt;X*F)&amp;*F:EZV&lt;1!!!!!!!%)!!!!!!!!!!1!!!'^16%AQ!!!!!!!!!!!!!Q!!!!!!!!A!!!!"!!!!!!!!"X9!!"FX?*S^77NQ%V550JOG:&gt;/(X:1CT1S1J7YC6FI9F)=&amp;3K(B5="#,=]+;C!,2*I'E\4A9Q#&gt;5#U05=!SQUT^C=YY!Q\]Y+?W"A3D)`#$5@A2Y)?-QYD_19KQ8=`&gt;T4[&lt;B&amp;A\;7@OX'4O_=[^Z`P//@?W!)YQ5W%:B.U#5-R^H#Q8I.!8JQ"CV41E@^R&gt;Q+SG(A&amp;6\K!%K+&gt;8-T=MA^1Y!9J^=4=^V&gt;-.@_&amp;K];:YB,I%^=QN8&amp;L!/"#M5)"38XSM@3G89,D4Y\DO@!86$O/:1^3A:2XH@%!@CA&lt;2)51HE&gt;&amp;?41U#Z:FIN59L6XL&lt;_3B(PL66UQY*UC9!YYG8B,D%MYC)LPNFS&amp;\K&amp;TCF1!*#4I+T:]^K2H&lt;:S#VN9R&lt;;Y(KA@I@?$$:F;"0G%J-FGU,:JB&gt;NEHY]TM'D:/`%S'Q[GIOD*:LN3J[9G#JG1LVE&gt;PPW&lt;44$-7GW49"S,L'!&gt;N#X@.@T3ZKDZY!#+P9[,?Y8@\7MB00W*E+#N,9-?8$65EQ,TFM%K)L',7_!6@&amp;V(4[87,!K,-R"&amp;KB&amp;%AMW4T=$'AV(HU#$RRKL(.`1VB'/]#%WO)8&gt;X/9.B^E&gt;)8_H.]+T0G`%/Z3AO:[Y&lt;39Z08%G;10'A*7["C@UQ1\#S:-H-1!Y;K:V;$K73[BW^O2JF)$\N)!4LVLEZG(E0!/LNJ,IO7INCG2NEG4H3_-+;?S8RDNEJ#:,]X:JX#3.17F]4Z0Y.*3YH4%@QS$R[3-P]2G?_0D&gt;:IHX5(XQ21;ZTJ3.&gt;",PQ_B&gt;B:Y-.L01:I^"YHXIZ[LC*Z0%8`,%':0'_R2@KM:\?HI-&gt;MB5L;JR+U8*'I^?&amp;B_,DYH3DYE*SX4Y2F+[4&lt;*I2DL)PLI1`WP9;!R^&gt;VJ&gt;&lt;]Q=?^9#ML:,6`'BA,`&gt;'`%(WROW?5-G,II%//#*/X$0J?C#[SYG(CJQTRS-QO-^0`5&amp;0.[.R'W\5UYX_@!0,B,TZ0R\X@S#&lt;H[?&gt;NC=GJOD[+;/3X4+*`G7_"G.@J[4`+#(%@'$)4O''4;.3R4JEF0WMVCB4L2,V)GCC$ZR6%*7!.'*]"'+FC0@[O8;9]L;-ARB&amp;3R2!"`@EQ#``/K5$+BJY4BKQ&lt;5?8+W!O8N?TNU]&gt;V&gt;?=I50%^)8JY%F!GB%!81FWUO?=JK^CA!]JZV,.161K)$[*SCA)+E!7QM@]/\9&amp;AS:]X#V!/^\%M[F=IA!ZE+6M;J'3?)X'&gt;1H`A'5/!DTMF3@,:T'.Q:V(Q:VN#[I)!7V3G7*E9*[ZMQ:$#K/GG(8U0+&lt;D\]O=*E9.NMC)2_KB"!SG&amp;&lt;]NE_!$_Q6J00%(&amp;0G,+B4NTRHSI)[VB^GP7S)X]+(_0&lt;.0"M*YE&gt;V29WNAGZVV6J*.3;&gt;;KU!_\&amp;KA(Z,*++DF)A?4"F2Q)D7`&gt;_)XB4AE#^O&gt;7#B,N26#QN5)LH4&amp;03`D&gt;6/L&gt;3!IM@0NOJIR&amp;5TV;=&amp;_W.-7T.,,,!+XHWJZ'K;,R(A-&amp;L-Y",&amp;/HIM_$M"*BAXI&lt;*D0#*O",_T6=&gt;K#L7T;G8E%RH&gt;)K&amp;8V".U'R1"!W6*-&amp;*!,%JR%(&gt;IB5*M-R;E4W7E0!GJ@#^"9B"J&lt;"KEI!ZJOQY*288%)#J8&lt;&lt;[\S_*GQ4V2&gt;&amp;?#`3\#A/UOX;):@+9;=!/C&lt;&amp;/!'NJ8XB`;@/KXQ]G&amp;+\&amp;V__)&amp;]F567RX=B9!E*VK25S2N?QBE+3?GK3/QC1_NW)+^I&lt;W&gt;&lt;QM0665(KIJ5V%)&gt;I?6)[#39L?4&lt;1%X'CFI7?Y&lt;'EE6E$EJRF83W*?GF7)"/O7T,P.)&amp;=J7NA`'Q4'V$7&amp;]V4P,9G&amp;-_&lt;"'52&amp;FJS53SB+F-&amp;O6`[%K:MN2,'[,HD%N62H%X/_8&gt;S.KQEHM$/(%X6=0@T=0M&gt;`01M"N5[CZ^BSYBGRG,Y83H[.$ZCE1(JGJS(:CCG^@IZN7[,HL&amp;[0*N@&lt;?'PET&gt;/OG4V!,(@ZGK\L$+P*/SQ=ZZ=I.^6^&gt;A^XQHJ^)I@@IFF`&lt;D"6B^^NX&amp;"NK)6TF$M^O7.J?KMXB#U#V]*/*PXWL/(Q4W$\W1D)%#W!$(-B:F&amp;-8?&amp;[V+7&gt;:#^;9J6+2TP[&lt;&gt;A2_EP-ZCJ,&lt;L)F6]4YY5L6V&amp;GP'V1/#/)VQ@($2=1[FV;7-T0=M[5^D#B`T?.L9J7$0E"EKNT]E.F'L.T1W5?D8&gt;$@2!ZBNIGNMHN3&amp;F=BR]9H*1'V0=0GV[SO%/Q1MAXH%TZ36J+:_&gt;\5XFZ;#04]0Y5\FBP$2(D$-DT,B^O)S8J7#]U-#Y^.:O1,S!C8'YG*&lt;RT6ES&lt;F`E$Q6W?E-]_QL@[1`DCX0I;`.3&lt;F[&lt;0_4IN2E@7?&lt;BRW%S$T_F9,\)Q0Q+AO&gt;!P!9T][P4-N_=Z2OTV"0S&gt;`+B$,SPS1XP;X0%_\I2ZHX^=(FP4=&amp;\M9(X_?32_3@C/=S],U\,_VN::HT:GM;7_=B[/.A2QN&gt;RET&gt;A@J9C*UNS1XVDDKB@/M,5,RMO^=N45&amp;]C57^LUPV*F,ZRI50^6Q@&gt;QHH%4!O9B1C_2Y!RHI;SHZF'$&amp;9RX5AP:(INAR1FQ..U,XU`&gt;EX^:UBMO7I:P5Q^SLP#H8$3&amp;@]#-P2I(1!!!!!!"!!!!(U!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!!%!!!!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!8.!!!!#!!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!&amp;,'1#!!!!!!!%!#!!Q`````Q!"!!!!!!%P!!!!#Q!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!$E"4#&amp;.F&gt;(2J&lt;G&gt;T!!!&lt;1"9!!1FT:7VB='BP=G5!#8.F&lt;7&amp;Q;'^S:1!R!0(#=G/O!!!!!2"4:7VB='BP=G5A5G6G4H6N!"B!=!!"!!%!"QF4:7VB='BP=G5!'%!Q`````Q^5:8*N;7ZB&gt;'FP&lt;E.I98)!+E"1!!E!!!!"!!)!!Q!%!!5!"A!)!!E14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!+!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!%E:!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!E!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'!!!!"Q!!!!A!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E:!)!!!!!!!1!&amp;!!=!!!%!!.ZJQI!!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2E!A!!!!!!"!!5!"Q!!!1!!XGH#A!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!&amp;,'1#!!!!!!!%!#!!Q`````Q!"!!!!!!%P!!!!#Q!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!$E"4#&amp;.F&gt;(2J&lt;G&gt;T!!!&lt;1"9!!1FT:7VB='BP=G5!#8.F&lt;7&amp;Q;'^S:1!R!0(#=G/O!!!!!2"4:7VB='BP=G5A5G6G4H6N!"B!=!!"!!%!"QF4:7VB='BP=G5!'%!Q`````Q^5:8*N;7ZB&gt;'FP&lt;E.I98)!+E"1!!E!!!!"!!)!!Q!%!!5!"A!)!!E14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!+!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:'1#!!!!!!!%!"1!$!!!"!!!!!!!B!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!7A:!)!!!!!!#Q!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!$E"4#&amp;.F&gt;(2J&lt;G&gt;T!!!&lt;1"9!!1FT:7VB='BP=G5!#8.F&lt;7&amp;Q;'^S:1!R!0(#=G/O!!!!!2"4:7VB='BP=G5A5G6G4H6N!"B!=!!"!!%!"QF4:7VB='BP=G5!'%!Q`````Q^5:8*N;7ZB&gt;'FP&lt;E.I98)!+E"1!!E!!!!"!!)!!Q!%!!5!"A!)!!E14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!!!1!+!!!!!!!!!!!!!!!!!!!!!!!!!!!!'1#!!!!!!!%!"!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6'1#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!!!!!"!!.!"%!!!!%!!!"81!!!#A!!!!#!!!%!!!!!!M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"WA!!!Z.YH)V4WU\55"2&gt;1W&gt;A/N;BD"71CV2"@3-9%R^]KG*)@(!AV0DMI4W&amp;E`1SHH:'(PEC0Y)0Y2PA#X2.TT#3/"'\E_;MN=^??_X&gt;&amp;-",L!@,;.CFT-4AL.!3&gt;Y[P=8/JIZ]!'GZYS`L(-OE0-[Q'!T19&gt;QMWALV@@(J@0Y8P?;]MBDK3@F^E%LR@ZR9`;D73GNG2+F729]XQ3Q&gt;+:T^%L4`*^%T'`FT%-P8\R3Y]QX2#K:5Q&amp;&amp;;#&amp;FQ;/J([-.E`%XEOUR,I"G%\F&amp;7F]F/C`ZREGJK[`3*VJH*2U1_V.&lt;\DZEJ&gt;8F$)=A`,3*FJ&gt;N.2KEZKYI-I*7'5CL*UJE25J8A8(-('(#QUU=)]&amp;N!G\GTOJ]/SYE;+R+_L`!%V238^7&amp;3#.VN!"T'&gt;8]-BM#91&lt;^#&amp;(3T!3N*4N)-,KTCO$#',"#[\78#RB*YVD"0WKA&gt;C\\$ODF@`7EE8:C8,`+2DV^YU($T'#DSL/I`JYYDT0-)K4^&amp;Y*;TQ:CTT\9T*V__&gt;X'C/N6KMZ9OC&lt;8QDV]1"/`NY3H^/(6NU@"P/*0ZG`G4]/MQ?'XAWU_(^X[&lt;*;G`'&lt;\"*`!!0M9AH7--']8.M9Y@[2H?(;*P&gt;8\$?LLFZ&gt;GWS_^RP?,&lt;.L1!!!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!(#Q!!!32!!!!)!!!(!Q!!!!!!!!!!!!!!#!!!!!U!!!%B!!!!"V-35*/!!!!!!!!!7R-6F.3!!!!!!!!!9"36&amp;.(!!!!!!!!!:2$1V.5!!!!!!!!!;B-38:J!!!!!!!!!&lt;R$4UZ1!!!!!!!!!&gt;"544AQ!!!!!1!!!?2%2E24!!!!!!!!!AR-372T!!!!!!!!!C"735.%!!!!!A!!!D2W:8*T!!!!"!!!!H"41V.3!!!!!!!!!N2(1V"3!!!!!!!!!OB*1U^/!!!!!!!!!PRJ9WQY!!!!!!!!!R"-37:Q!!!!!!!!!S2'5%6Y!!!!!!!!!TB'5%BC!!!!!!!!!UR'5&amp;.&amp;!!!!!!!!!W"75%21!!!!!!!!!X2-37*E!!!!!!!!!YB#2%6Y!!!!!!!!!ZR#2%BC!!!!!!!!!\"#2&amp;.&amp;!!!!!!!!!]273624!!!!!!!!!^B%6%B1!!!!!!!!!_R.65F%!!!!!!!!"!")36.5!!!!!!!!""271V21!!!!!!!!"#B'6%&amp;#!!!!!!!!"$Q!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#Q!!!!!!!!!!0````]!!!!!!!!!U!!!!!!!!!!!`````Q!!!!!!!!$E!!!!!!!!!!$`````!!!!!!!!!/Q!!!!!!!!!!0````]!!!!!!!!"@!!!!!!!!!!!`````Q!!!!!!!!'%!!!!!!!!!!,`````!!!!!!!!!&lt;!!!!!!!!!!!0````]!!!!!!!!"S!!!!!!!!!!!`````Q!!!!!!!!)9!!!!!!!!!!$`````!!!!!!!!!CA!!!!!!!!!!@````]!!!!!!!!$M!!!!!!!!!!#`````Q!!!!!!!!4I!!!!!!!!!!4`````!!!!!!!!"@A!!!!!!!!!"`````]!!!!!!!!'$!!!!!!!!!!)`````Q!!!!!!!!9=!!!!!!!!!!H`````!!!!!!!!"D!!!!!!!!!!#P````]!!!!!!!!'1!!!!!!!!!!!`````Q!!!!!!!!:5!!!!!!!!!!$`````!!!!!!!!"GQ!!!!!!!!!!0````]!!!!!!!!'A!!!!!!!!!!!`````Q!!!!!!!!=%!!!!!!!!!!$`````!!!!!!!!#QA!!!!!!!!!!0````]!!!!!!!!,E!!!!!!!!!!!`````Q!!!!!!!!O=!!!!!!!!!!$`````!!!!!!!!%RA!!!!!!!!!!0````]!!!!!!!!4)!!!!!!!!!!!`````Q!!!!!!!"-I!!!!!!!!!!$`````!!!!!!!!%TA!!!!!!!!!!0````]!!!!!!!!41!!!!!!!!!!!`````Q!!!!!!!"/I!!!!!!!!!!$`````!!!!!!!!%\!!!!!!!!!!!0````]!!!!!!!!:B!!!!!!!!!!!`````Q!!!!!!!"G-!!!!!!!!!!$`````!!!!!!!!':1!!!!!!!!!!0````]!!!!!!!!:Q!!!!!!!!!#!`````Q!!!!!!!"OA!!!!!!R0=W.J1G&amp;T:3ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!!!!E!!1!!!!!!!!%!!!!"!"R!5!!!&amp;62F;X2S&lt;WZJ?%*B=W5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!!!!%!!!!!!!!#!!!!!1!=1&amp;!!!"65:7NU=G^O;8B#98.F,GRW9WRB=X-!!1!!!!!!!@````Y!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!:!)!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!!"!"R!5!!!&amp;62F;X2S&lt;WZJ?%*B=W5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!"`````A!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!!!"!!!!!!!"!!!!!!)!(%!Q`````R*736.")&amp;*F=W^V=G.F)%ZB&lt;75!!'%!]&gt;L7X.1!!!!$%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=QR0=W.J1G&amp;T:3ZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!!!"!!!!!!!#!!!!!!9!(%!Q`````R*736.")&amp;*F=W^V=G.F)%ZB&lt;75!!"B!-0````]02(*J&gt;G6S)&amp;*F&gt;GFT;7^O!"J!-0````]22GFS&lt;8&gt;B=G5A5G6W;8.J&lt;WY!%E!Q`````QF.&lt;W2F&lt;#"/&lt;SY!&amp;%!Q`````QJ4:8*J97QA4G]O!!"J!0(;VN]-!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X--4X.D;5*B=W5O9X2M!$*!5!!&amp;!!!!!1!#!!-!""V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!5!!!!&amp;!!!!!0````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!!!!%!!!!!!!-!!!!!"Q!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!!Z!5QB4:82U;7ZH=Q!!;Q$RX7$*4!!!!!-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T$%^T9WF#98.F,G.U&lt;!!U1&amp;!!"A!!!!%!!A!$!!1!"2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!9!!!!'!!!!!!!!!!%!!!!#!!!!!Q!!!!4`````!!!!!!!!!!!!!!!!!!!!!!!!!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!!!!%!!!!!!!1!!!!!#!!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!$E"4#&amp;.F&gt;(2J&lt;G&gt;T!!"N!0(&gt;9-I.!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X--4X.D;5*B=W5O9X2M!$:!5!!(!!!!!1!#!!-!"!!&amp;!!9&gt;1WRV=X2F=C"P:C"D&lt;'&amp;T=S"Q=GFW982F)'2B&gt;'%!!1!(!!!!"Q!!!!!!!!!"!!!!!A!!!!-!!!!%`````Q!!!!5!!!!!!!!!!!!!!!!!!!!!!!!!!!!:!)!!!!!!!1!%!!!!!1!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!!!!!"!!!!!!!&amp;!!!!!!I!(%!Q`````R*736.")&amp;*F=W^V=G.F)%ZB&lt;75!!"B!-0````]02(*J&gt;G6S)&amp;*F&gt;GFT;7^O!"J!-0````]22GFS&lt;8&gt;B=G5A5G6W;8.J&lt;WY!%E!Q`````QF.&lt;W2F&lt;#"/&lt;SY!&amp;%!Q`````QJ4:8*J97QA4G]O!!!81!5!%%ZV&lt;7*F=E^G1WBB&lt;GZF&lt;(-!!!Z!5QB4:82U;7ZH=Q!!'U!7!!%*=W6N98"I&lt;X*F!!FT:7VB='BP=G5!-1$RQH*DLA!!!!%15W6N98"I&lt;X*F)&amp;*F:EZV&lt;1!91(!!!1!"!!=*5W6N98"I&lt;X*F!']!]&gt;VG'&amp;5!!!!$%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=QR0=W.J1G&amp;T:3ZD&gt;'Q!/%"1!!A!!!!"!!)!!Q!%!!5!"A!)(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!#1!!!!A!!!!!!!!!!1!!!!)!!!!$!!!!"!!!!!5!!!!'`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!"E!A!!!!!!"!!1!!!!"!!!!!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!'1#!!!!!!!!!!!!!!!!!!!%!!!!!!!9!!!!!#Q!=1$$`````%F:*5U%A5G6T&lt;X6S9W5A4G&amp;N:1!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!31$$`````#5VP:'6M)%ZP,A!51$$`````#F.F=GFB&lt;#"/&lt;SY!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!$E"4#&amp;.F&gt;(2J&lt;G&gt;T!!!&lt;1"9!!1FT:7VB='BP=G5!#8.F&lt;7&amp;Q;'^S:1!R!0(#=G/O!!!!!2"4:7VB='BP=G5A5G6G4H6N!"B!=!!"!!%!"QF4:7VB='BP=G5!'%!Q`````Q^5:8*N;7ZB&gt;'FP&lt;E.I98)!=1$RXAM5D!!!!!-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T$%^T9WF#98.F,G.U&lt;!![1&amp;!!#1!!!!%!!A!$!!1!"1!'!!A!#2V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!I!!!!*!!!!!!!!!!%!!!!#!!!!!Q!!!!1!!!!&amp;!!!!"A!!!!@`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!'1#!!!!!!!%!"!!!!!%!!!!!!!!!!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!"E!A!!!!!!!!!!!!!!#!!!!+V2F;X2S&lt;WZJ?%2S;8:F=CZM&gt;GRJ9DJ5:7NU=G^O;8B#98.F,GRW9WRB=X-!!!!G6'6L&gt;(*P&lt;GFY2(*J&gt;G6S,GRW&lt;'FC/E^T9WF#98.F,GRW9WRB=X-</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Property Name="NI_IconEditor" Type="Str">49 57 48 49 56 48 48 50 13 0 0 0 0 1 23 21 76 111 97 100 32 38 32 85 110 108 111 97 100 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 9 0 0 13 45 1 100 1 100 80 84 72 48 0 0 0 4 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 15 13 76 97 121 101 114 46 108 118 99 108 97 115 115 0 0 1 0 0 0 0 0 7 0 0 12 185 0 0 0 0 0 0 0 0 0 0 12 158 0 40 0 0 12 152 0 0 12 0 0 0 0 0 0 32 0 32 0 24 0 0 0 0 0 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 7 86 73 32 73 99 111 110 100 1 0 2 0 0 0 8 79 115 99 105 66 97 115 101 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 11 83 109 97 108 108 32 70 111 110 116 115 0 1 9 1 0

</Property>
	<Item Name="OsciBase.ctl" Type="Class Private Data" URL="OsciBase.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Accessors" Type="Folder">
		<Item Name="Driver Revision" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Driver Revision</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Driver Revision</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Driver Revision.vi" Type="VI" URL="../Read Driver Revision.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"B!-0````]02(*J&gt;G6S)&amp;*F&gt;GFT;7^O!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Driver Revision.vi" Type="VI" URL="../Write Driver Revision.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%^!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!'%!Q`````Q^%=GFW:8)A5G6W;8.J&lt;WY!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Firmware Revision" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Firmware Revision</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Firmware Revision</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Firmware Revision.vi" Type="VI" URL="../Read Firmware Revision.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"J!-0````]22GFS&lt;8&gt;B=G5A5G6W;8.J&lt;WY!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Firmware Revision.vi" Type="VI" URL="../Write Firmware Revision.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%`!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!'E!Q`````R&amp;';8*N&gt;W&amp;S:3"3:8:J=WFP&lt;A!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Model No_" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Model No_</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Model No_</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Model No_.vi" Type="VI" URL="../Read Model No_.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%X!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"*!-0````]*47^E:7QA4G]O!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Model No_.vi" Type="VI" URL="../Write Model No_.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%X!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!%E!Q`````QF.&lt;W2F&lt;#"/&lt;SY!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="NumberOfChannels" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">NumberOfChannels</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">NumberOfChannels</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read NumberOfChannels.vi" Type="VI" URL="../Read NumberOfChannels.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"&gt;!"1!14H6N9G6S4W:$;'&amp;O&lt;G6M=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write NumberOfChannels.vi" Type="VI" URL="../Write NumberOfChannels.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!&amp;U!&amp;!""/&gt;7VC:8*0:E.I97ZO:7RT!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!'%!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Serial No_" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Serial No_</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Serial No_</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Serial No_.vi" Type="VI" URL="../Read Serial No_.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"2!-0````]+5W6S;7&amp;M)%ZP,A!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Serial No_.vi" Type="VI" URL="../Write Serial No_.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Z!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!&amp;%!Q`````QJ4:8*J97QA4G]O!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!B!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Settings" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Settings</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Settings</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Settings.vi" Type="VI" URL="../Read Settings.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write Settings.vi" Type="VI" URL="../Write Settings.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!5QB4:82U;7ZH=Q!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8388608</Property>
			</Item>
		</Item>
		<Item Name="TerminationChar" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">TerminationChar</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TerminationChar</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read TerminationChar.vi" Type="VI" URL="../Read TerminationChar.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"B!-0````]06'6S&lt;7FO982J&lt;WZ$;'&amp;S!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Write TerminationChar.vi" Type="VI" URL="../Write TerminationChar.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!'%!Q`````Q^5:8*N;7ZB&gt;'FP&lt;E.I98)!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="VISA Resource Name" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VISA Resource Name</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VISA Resource Name</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read VISA Resource Name.vi" Type="VI" URL="../Read VISA Resource Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"R!-0````]36EF413"3:8.P&gt;8*D:3"/97VF!!![1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!$%^T9WF#98.F)'^V&gt;!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
			<Item Name="Write VISA Resource Name.vi" Type="VI" URL="../Write VISA Resource Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;/!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!(%!Q`````R*736.")&amp;*F=W^V=G.F)%ZB&lt;75!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!#%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1115684864</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Protected" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
		<Item Name="InitSetting.vi" Type="VI" URL="../InitSetting.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)N!!!!&amp;!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!!1!!!!31$$`````#5FO;82797RV:1!-1#%(17.U;8:F0Q!31$$`````#%.N:%&gt;S&lt;X6Q!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!&amp;!!9!"QB&amp;=H*P=C"*&lt;A!!%E!Q`````QF$&lt;WZG;7&gt;$&lt;71!%E!Q`````QB2&gt;76S?5.N:!!!&amp;E!Q`````QV198*B&lt;76U:8*/97VF!!V!!Q!(1WBB&lt;GZF&lt;!!/1#%)1WBF9WNC&lt;XA!!":!1!!"`````Q!.#&amp;"P=X.J9GRF!!!11$$`````"F.U=GFO:Q!!'%"!!!(`````!!],5'^T=WFC&lt;'6398=!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!5!"A!(#56S=G^S)%^V&gt;!#:!0!!&amp;!!!!!%!!A!$!!1!#!!*!!I!#Q!-!!Y!%!!"!!%!%1!"!!%!!1!"!")#!!%1!!!3!!!!!!!!!AA!!!!)!!!##!!!!!A!!!))!!!##!!!!1I!!!!)!!!##!!!!AA!!!!!!!!!!!!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!$15!&amp;1!!!!!!!!!!!!!!!!!!!1!!!!!!!!!"!"-!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
		</Item>
		<Item Name="ReadFromDevice.vi" Type="VI" URL="../ReadFromDevice.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'F!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!$E!Q`````Q6797RV:1!/1&amp;-)5W6U&gt;'FO:X-!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!%!!!!$5!$!!&gt;$;'&amp;O&lt;G6M!":!-0````].5'&amp;S97VF&gt;'6S4G&amp;N:1!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!(E!Q`````R20&gt;G6S=GFE:3"%:8:J9W63:8"M?1!!&amp;E!B%%^W:8*S;72F)%&amp;D&gt;'FW:4]!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!91$Q!!Q!!Q!%!!5!"A!(!!=!#!!*!!I!#Q!-!!U$!!"Y!!!.#!!!#1!!!!E!!!!.#Q!!!!!!!!!!!!!)!!!"#A!!!!I!!!%+!!!!#A!!!")!!!U!!!!-!!!!!!!!!!!!!!%!$A!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
		</Item>
		<Item Name="Set NameRaw.vi" Type="VI" URL="../Set NameRaw.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!%%!Q`````Q:4&gt;(*J&lt;G=!!"2!1!!"`````Q!("UZB&lt;76398=!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1)!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!))!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!+!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
		</Item>
		<Item Name="UpdateSettings.vi" Type="VI" URL="../UpdateSettings.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'8!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!!V!!Q!(1WBB&lt;GZF&lt;!!71$$`````$6"B=G&amp;N:82F=EZB&lt;75!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!!R!)1&gt;#&lt;W^M:7&amp;O!"R!1!!"`````Q!+$V"P=X.J9GRF)&amp;:B&lt;(6F=Q!-1#%(17.U;8:F0Q!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!=!#!!*!!M!$!!.!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!#!!!!AA!!!!)!!!##!!!!!A!!!!3!!!.!!!!$!!!!!!!!!!!!!!"!!Y!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
		</Item>
		<Item Name="Write2Device.vi" Type="VI" URL="../Write2Device.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'6!!!!$Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!$E!B#&amp;.V9W.F=X-`!!!/1&amp;-)5W6U&gt;'FO:X-!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!%!!!!$5!$!!&gt;$;'&amp;O&lt;G6M!":!-0````].5'&amp;S97VF&gt;'6S4G&amp;N:1!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!%E!B$&amp;&amp;V:8*Z5G6T&gt;7RU0Q!!%E!Q`````QB/:8&gt;797RV:1!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"1!'!!=!"Q!)!!E!#A!,!!Q!$1-!!(A!!!U)!!!*!!!!#1!!!!U,!!!!!!!!!!!!!!A!!!%+!!!!#A!!!!A!!!%+!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!/!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
		</Item>
	</Item>
	<Item Name="Public" Type="Folder">
		<Item Name="Basic" Type="Folder">
			<Item Name="Clear.vi" Type="VI" URL="../Clear.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Close.vi" Type="VI" URL="../Close.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Error Query.vi" Type="VI" URL="../Error Query.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;(!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!&amp;E!Q`````QV&amp;=H*P=C".:8.T97&gt;F!!R!)1:&amp;=H*P=D]!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!%!!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E#!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Initialize Device.vi" Type="VI" URL="../Initialize Device.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"&amp;!"1!,4G^G1WBB&lt;GZF&lt;(-!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!/1#%*5G6T:81A+%9J!"*!)1R*2#"2&gt;76S?3!I2CE!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!)!!!!#!!!!*!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Initialize Settings.vi" Type="VI" URL="../Initialize Settings.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%W!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!%5!&amp;!!N/&lt;W:$;'&amp;O&lt;G6M=Q!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Initialize Static Cmds.vi" Type="VI" URL="../Initialize Static Cmds.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Initialize.vi" Type="VI" URL="../Initialize.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"&amp;!"1!,4G^G1WBB&lt;GZF&lt;(-!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!/1#%*5G6T:81A+%9J!"*!)1R*2#"2&gt;76S?3!I2CE!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I$!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!)!!!!#!!!!")!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.MustCallParent" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MustOverride" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
			</Item>
			<Item Name="Reset.vi" Type="VI" URL="../Reset.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Revision Query.vi" Type="VI" URL="../Revision Query.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Self-Test.vi" Type="VI" URL="../Self-Test.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;7!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!'E!Q`````R&amp;T:7RG)(2F=X1A&lt;76T=W&amp;H:1!81!=!%(.F&lt;'9A&gt;'6T&gt;#"S:8.V&lt;(1!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!%!!!!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!6!$Q!!Q!!Q!%!!5!"A!(!!=!"Q!(!!A!"Q!(!!E#!!"Y!!!.#!!!#1!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Configuration" Type="Folder">
			<Item Name="Acquisition" Type="Folder">
				<Item Name="Configure Acquisition.vi" Type="VI" URL="../Configure Acquisition.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!0D!!!!%1!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!4E!]1!!!!!!!!!$%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=QF51G&amp;T:3ZD&gt;'Q""5!7!#E&amp;.$!A=(-'-4!Q)("T"D)Q-#"Q=Q9U-$!A=(-%-3"O=Q1S)'ZT"$1A&lt;H-&amp;-4!A&lt;H-&amp;-D!A&lt;H-&amp;.$!A&lt;H-'-4!Q)'ZT"D)Q-#"O=Q9U-$!A&lt;H-%-3"V=Q1S)(6T"$1A&gt;8-&amp;-4!A&gt;8-&amp;-D!A&gt;8-&amp;.$!A&gt;8-'-4!Q)(6T"D)Q-#"V=Q9U-$!A&gt;8-%-3"N=Q1S)'VT"$1A&lt;8-&amp;-4!A&lt;8-&amp;-D!A&lt;8-&amp;.$!A&lt;8-'-4!Q)'VT"D)Q-#"N=Q9U-$!A&lt;8-$-3"T!T)A=Q-U)(-%-4!A=Q1S-#"T"$1Q)(-&amp;-4!Q)(-&amp;-D!Q)(-&amp;.$!Q)(-%-3"L=Q!!#&amp;2J&lt;76#98.F!!#"!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-,5ERF&lt;G&gt;U;#ZD&gt;'Q!3U!7!!U%186U&lt;Q-V-$!#-7M#-GM$-4"L!T)Q;Q1R-$"L"$)Q-'M#-5U#-EU#.5U$-4".!T)Q41!-5G6D&lt;X*E4'6O:X2I!!!41!I!$62S;7&gt;H:8)A2'6M98E!&amp;%!B$E2F&lt;'&amp;Z)%^O)#]A4W:G!!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!&amp;!!9!"QB&amp;=H*P=C"*&lt;A!!$U!'!!B"&gt;G6S97&gt;F=Q!!'5!+!".1&lt;X.J&gt;'FP&lt;C"J&lt;C"1:8*D:7ZU!!^!"A!*27ZW:7RP='6T!(9!]1!!!!!!!!!$%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=QB5?8"F,G.U&lt;!"$1"9!"1:497VQ&lt;'5%5'6B;QB&amp;&lt;H:F&lt;'^Q:1&gt;"&gt;G6S97&gt;F"UBJ:WB3:8-!!"""9X&amp;V;8.J&gt;'FP&lt;C"5?8"F!!!%!!!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!5!"A!(#56S=G^S)%^V&gt;!#%!0!!&amp;!!!!!%!!A!$!!1!#!!*!!I!#Q!-!!U!$1!.!!U!$A!.!!U!$1!.!!]#!!%1!!#1!!!!#!!!!!A!!!!)!!!!#!!!!!A!!!!)!!!!#!!!!!A!!!!)!!!!!!!!!!!!!!!!!!!!!!!!!)U!!!!!!!!!!!!!!!!!!!!!!!!!$15!!!!"!"!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Fetch Waveform.vi" Type="VI" URL="../Fetch Waveform.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(.!!!!%1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!%5!+!!N9)%FO9X*F&lt;76O&gt;!!01!I!#5FO;82J97QA7!![1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!$%^T9WF#98.F)'^V&gt;!!!$5!+!!&gt;/&gt;7VF=GFD!"R!1!!"`````Q!($F&gt;B&gt;G6G&lt;X*N)%&amp;S=G&amp;Z!!!41!=!$5&amp;D&gt;(6B&lt;#"1&lt;WFO&gt;(-!$%!B"UVB&lt;H6B&lt;$]!'5!$!"*898:F:G^S&lt;3"4&gt;'&amp;S&gt;#!I-3E!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!&lt;1!-!&amp;&amp;&gt;B&gt;G6G&lt;X*N)%6O:#!I-4!Q-$!J!!!11$$`````"F.P&gt;8*D:1!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!A!#1!+!!M!$!!.!!Y!$Q)!!(A!!!U)!!!*!!!!#1!!!)U,!!!*!!!!#1!!!!A!!!!)!!!!#!!!!!A!!!))!!!!E!!!!!!"!"!!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Start Measurement.vi" Type="VI" URL="../Start Measurement.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">1115685376</Property>
				</Item>
			</Item>
			<Item Name="Channel" Type="Folder">
				<Item Name="Configure Channel Names.vi" Type="VI" URL="../Configure Channel Names.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;)!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!&amp;E!Q`````QR$;'&amp;O&lt;G6M)%ZB&lt;75!!!V!"1!(1WBB&lt;GZF&lt;!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!"!!!!#1!!!!!!%!#A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
				<Item Name="Configure Channel.vi" Type="VI" URL="../Configure Channel.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!.Z!!!!%Q!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;E!]1!!!!!!!!!$%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=QN&amp;&lt;G&amp;C&lt;'6E,G.U&lt;!!D1"9!!A.0:G9#4WY!!!^$;'&amp;O&lt;G6M)%6O97*M:71!D!$R!!!!!!!!!!-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T%%VB?%:S:8&amp;V:7ZD?3ZD&gt;'Q!55!7!!=%2H6*&lt;!2'&gt;7RM"T-V-#".3(I(-D5Q)%V)?A=S-$!A45B["T%V-#".3(I'-D!A45B[!!!447&amp;Y)%FO=(6U)%:S:8&amp;V:7ZD?1"J!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-.37VQ:72B&lt;G.F,G.U&lt;!!R1"9!!Q9R)%V0;'U'.T5A4WBN"D5Q)%^I&lt;1!!$UFO=(6U)%FN='6E97ZD:1"G!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-16G6S&gt;%.P&gt;8"M;7ZH,G.U&lt;!!L1"9!!Q*"1Q*%1Q:(=G^V&lt;G1!!"&amp;7:8*U;7.B&lt;#"$&lt;X6Q&lt;'FO:Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!&amp;!!9!"QB&amp;=H*P=C"*&lt;A!!&amp;U!+!"&amp;1=G^C:3""&gt;(2F&lt;H6B&gt;'FP&lt;A!.1!5!"U.I97ZO:7Q!&amp;5!+!!Z7:8*U;7.B&lt;#"397ZH:1!!&amp;5!+!!Z1&lt;X.J&gt;'FP&lt;C!I2%F7+1!!&amp;5!+!!^7:8*U;7.B&lt;#"0:G:T:81!$5!+!!:%26.,:8=!!!1!!!![1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!$%^T9WF#98.F)'^V&gt;!!!&amp;E"1!!-!"1!'!!=*28*S&lt;X)A4X6U!)1!]!!5!!!!!1!#!!-!"!!)!!E!#A!,!!Q!$1!/!!]!$Q!1!!]!$Q!0!!]!%1)!!2!!!*!!!!!)!!!!#!!!!!A!!!!)!!!!#!!!!!A!!!!1!!!!#!!!!!A!!!!)!!!!#!!!!!!!!!!!!!!!D1!!!!!!!!!!!!!!!!!!!!!!!!!."1!!!!%!%A!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
			<Item Name="Trigger" Type="Folder">
				<Item Name="Configure Trigger.vi" Type="VI" URL="../Configure Trigger.vi">
					<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)\!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!"E!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-+6&amp;.M&lt;X"F,G.U&lt;!!P1"9!!Q23;8.F"%:B&lt;'Q,5GFT:3!P)%:B&lt;'Q!$62S;7&gt;H:8)A5WRP='5!&amp;E"1!!-!!!!"!!))28*S&lt;X)A37Y!!".!#A!.6(*J:W&gt;F=C"-:8:F&lt;!#@!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-,6&amp;.P&gt;8*D:3ZD&gt;'Q!;5!7!!I$1UAR!U.)-A.$3$-$1UAU#%6Y&gt;'6S&lt;G&amp;M$56Y&gt;'6S&lt;G&amp;M)#]A-4!(15-A4'FO:1B7:8*U;7.B&lt;!N"&lt;(2F=GZB&gt;'FO:QF"&gt;8BJ&lt;'FB=HE!$F2S;7&gt;H:8)A5W^V=G.F!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!9!"!!(!!A!#1!+!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!#!!!!!!!!!!)!!!!#!!!!!A!!!#1!!!!!!%!#Q!!!!!</Property>
					<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
					<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
					<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
					<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
					<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
					<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
				</Item>
			</Item>
		</Item>
		<Item Name="Status" Type="Folder">
			<Item Name="Query Acq Status.vi" Type="VI" URL="../Query Acq Status.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%X!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!"*!)1R"9X%A=X2P=("F:$]!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!E!!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
			</Item>
			<Item Name="Query Acquisition.vi" Type="VI" URL="../Query Acquisition.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Query All Settings.vi" Type="VI" URL="../Query All Settings.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
			</Item>
			<Item Name="Query Channel Name.vi" Type="VI" URL="../Query Channel Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!.1!9!"U.I97ZO:7Q!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!1!!!!E!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Query Channel.vi" Type="VI" URL="../Query Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;!!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!.1!9!"U.I97ZO:7Q!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!A!#1)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Query Trigger.vi" Type="VI" URL="../Query Trigger.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!!Z!5QB4:82U;7ZH=Q!!/E"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!R0=W.J1G&amp;T:3"P&gt;81!!":!5!!$!!!!!1!##%6S=G^S)%FO!!!Y1(!!(A!!)R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-!#U^T9WF#98.F)'FO!&amp;1!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!#1!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
		</Item>
		<Item Name="Utility" Type="Folder">
			<Item Name="Choose ChildClass.vi" Type="VI" URL="../Choose ChildClass.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%A!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
			</Item>
			<Item Name="Lock.vi" Type="VI" URL="../Lock.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="LockCommunication.vi" Type="VI" URL="../LockCommunication.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%S!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
			</Item>
			<Item Name="Set Data Encoding.vi" Type="VI" URL="../Set Data Encoding.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">128</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Set Display Intensity.vi" Type="VI" URL="../Set Display Intensity.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%Y!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!%U!&amp;!!V*&lt;H2F&lt;H.J&gt;(EA+$%J!$B!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!,4X.D;5*B=W5A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="Unlock.vi" Type="VI" URL="../Unlock.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%F!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396800</Property>
			</Item>
			<Item Name="UnlockCommunication.vi" Type="VI" URL="../UnlockCommunication.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%S!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AF&amp;=H*P=C"0&gt;81!"!!!!$J!=!!?!!!D%%^T9WF%=GFW:8)O&lt;(:M;7)14X.D;5*B=W5O&lt;(:D&lt;'&amp;T=Q!-4X.D;5*B=W5A&lt;X6U!!!71&amp;!!!Q!!!!%!!AB&amp;=H*P=C"*&lt;A!!/%"Q!"Y!!#-14X.D;52S;8:F=CZM&gt;GRJ9B"0=W.J1G&amp;T:3ZM&gt;G.M98.T!!N0=W.J1G&amp;T:3"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!1!"Q-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">8396816</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="Enabled.ctl" Type="VI" URL="../Enabled.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!R!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Impedance.ctl" Type="VI" URL="../Impedance.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!S!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="MaxFrequency.ctl" Type="VI" URL="../MaxFrequency.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!T!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="RLength.ctl" Type="VI" URL="../RLength.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!U!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="TBase.ctl" Type="VI" URL="../TBase.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!V!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="TSlope.ctl" Type="VI" URL="../TSlope.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!W!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="TSource.ctl" Type="VI" URL="../TSource.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!X!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="Type.ctl" Type="VI" URL="../Type.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!Y!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="VertCoupling.ctl" Type="VI" URL="../VertCoupling.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">'1#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"%!!!!!1!]!0%!!!!!!!!!!R"0=W.J2(*J&gt;G6S,GRW&lt;'FC%%^T9WF#98.F,GRW9WRB=X-*1W^O&gt;(*P&lt;#!Z!!A!!!!"!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">5242880</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
</LVClass>
